import { Module } from '@nestjs/common';
import { MyOtherLibService } from './my-other-lib.service';

@Module({
  providers: [MyOtherLibService],
  exports: [MyOtherLibService],
})
export class MyOtherLibModule {}

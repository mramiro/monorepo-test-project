import { Test, TestingModule } from '@nestjs/testing';
import { MyOtherLibService } from './my-other-lib.service';

describe('MyOtherLibService', () => {
  let service: MyOtherLibService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MyOtherLibService],
    }).compile();

    service = module.get<MyOtherLibService>(MyOtherLibService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

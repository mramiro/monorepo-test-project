module.exports = {
  "root": true,
  "ignorePatterns": ["**/*"],
  "plugins": ["@nrwl/nx"],
  "overrides": [
    {
      "files": ["*.ts", "*.tsx", "*.js", "*.jsx"],
      "rules": {
        "@nrwl/nx/enforce-module-boundaries": [
          "error",
          {
            "enforceBuildableLibDependency": true,
            "allow": [],
            "depConstraints": [
              {
                "sourceTag": "*",
                "onlyDependOnLibsWithTags": ["*"]
              }
            ]
          }
        ]
      }
    },
    {
      "files": ["*.ts", "*.tsx"],
      "excludedFiles": ["*.stories.tsx"],
      "extends": ["plugin:@nrwl/nx/react"],
      "rules": {
        "react/function-component-definition": [
          2,
          {
            "namedComponents": "function-declaration",
            "unnamedComponents": "arrow-function"
          }
        ],
        "react/jsx-fragments": ["error", "syntax"],
        "react-hooks/exhaustive-deps": "error"
      }
    },
    {
      "files": ["*.ts", "*.tsx"],
      "extends": ["plugin:@nrwl/nx/typescript"],
      "rules": {
        "@typescript-eslint/explicit-function-return-type": ["error"],
        "@typescript-eslint/no-explicit-any": ["error"],
        "no-unused-vars": "off",
        "@typescript-eslint/no-unused-vars": [
          "error",
          { "varsIgnorePattern": "^_", "argsIgnorePattern": "^_" }
        ],
        "comma-dangle": "off",
        "@typescript-eslint/comma-dangle": ["error"],
        "no-console": ["error"],
        "no-param-reassign": ["error"],
        "prefer-template": ["error"],
        "@typescript-eslint/array-type": [
          "error",
          { "default": "array-simple", "readonly": "generic" }
        ],
        "curly": ["error"],
        "react/jsx-no-useless-fragment": "off"
      }
    },
    {
      "files": ["*.js", "*.jsx"],
      "extends": ["plugin:@nrwl/nx/javascript"],
      "rules": {}
    }
  ]
}

import { Injectable } from '@nestjs/common';

@Injectable()
export class MyOtherAppService {
  getHello(): string {
    return 'Hello World!';
  }
}

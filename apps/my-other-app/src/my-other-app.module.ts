import { Module } from '@nestjs/common';
import { MyOtherAppController } from './my-other-app.controller';
import { MyOtherAppService } from './my-other-app.service';

@Module({
  imports: [],
  controllers: [MyOtherAppController],
  providers: [MyOtherAppService],
})
export class MyOtherAppModule {}

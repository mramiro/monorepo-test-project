import { Controller, Get } from '@nestjs/common';
import { MyOtherAppService } from './my-other-app.service';

@Controller()
export class MyOtherAppController {
  constructor(private readonly myOtherAppService: MyOtherAppService) {}

  @Get()
  getHello(): string {
    console.log("Test");
    return this.myOtherAppService.getHello();
  }
}

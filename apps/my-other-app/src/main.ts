import { NestFactory } from '@nestjs/core';
import { MyOtherAppModule } from './my-other-app.module';

async function bootstrap() {
  const app = await NestFactory.create(MyOtherAppModule);
  await app.listen(3000);
}
bootstrap();

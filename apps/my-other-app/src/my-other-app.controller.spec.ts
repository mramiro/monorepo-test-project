import { Test, TestingModule } from '@nestjs/testing';
import { MyOtherAppController } from './my-other-app.controller';
import { MyOtherAppService } from './my-other-app.service';

describe('MyOtherAppController', () => {
  let myOtherAppController: MyOtherAppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [MyOtherAppController],
      providers: [MyOtherAppService],
    }).compile();

    myOtherAppController = app.get<MyOtherAppController>(MyOtherAppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(myOtherAppController.getHello()).toBe('Hello World!');
    });
  });
});
